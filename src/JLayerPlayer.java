
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
 
import javax.sound.sampled.*;
 
public class JLayerPlayer {
	
	public static void main(String[] args) {
		System.out.println("Enter MP3: ");

		String MP3 = new Scanner(System.in).next();
		MP3 priMP3 = new MP3(new File(MP3));
		priMP3.play(100);
		System.out.println("Reached end");
		
		String input;
		for (int i = 0; i < 100; i++)
		{
		input = new Scanner(System.in).next();
		if (input.compareTo("pause") == 0) {
			priMP3.pause();
			System.out.println("Pausing MP3");
		}
		if (input.compareTo("unpause") == 0) {
			priMP3.unpause();
			System.out.println("Unpausing MP3");
		}
		if (input.compareTo("end") == 0) {
			priMP3.stop();
			System.out.println("Ending");
		}
		if (input.compareTo("restart") == 0) {
			double currTime = priMP3.getCurrentTime();
			priMP3.stop();
			priMP3 = new MP3(priMP3.getFile());
			priMP3.play(0);
			System.out.println("Restarting");
		}
		if (input.compareTo("time") == 0) {
			System.out.println(priMP3.getCurrentTime());
		}
		if (input.compareTo("playtime") == 0) {
			int newTime = new Scanner(System.in).nextInt();
			priMP3.stop();
			priMP3 = new MP3(priMP3.getFile());
			priMP3.play(newTime);
		}
		}
	}
 
}